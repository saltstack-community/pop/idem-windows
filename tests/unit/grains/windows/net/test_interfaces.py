import pytest


@pytest.mark.asyncio
async def test_load_interfaces(mock_hub, hub):
    class Interface:
        description = "iface Desc"
        macaddress = "00:00:00:00:01"
        ipenabled = 1
        ipsubnet = {"255.255.255.0", "64"}
        up = True
        defaultipgateway = {"192.168.2.1", "fe80::2"}
        ipaddress = {"192.168.2.1", "fe80::2"}

    mock_hub.exec.wmi.get.return_value = [Interface()]
    mock_hub.grains.windows.net.interfaces.load_interfaces = (
        hub.grains.windows.net.interfaces.load_interfaces
    )
    await mock_hub.grains.windows.net.interfaces.load_interfaces()

    assert mock_hub.grains.GRAINS.ip4_gw == ("192.168.2.1",)
    assert mock_hub.grains.GRAINS.ip6_gw == ("fe80::2",)
    assert mock_hub.grains.GRAINS.ip_gw is True

    assert mock_hub.grains.GRAINS.ipv4 == ("192.168.2.1",)
    assert mock_hub.grains.GRAINS.ipv6 == ("fe80::2",)
    assert mock_hub.grains.GRAINS.hwaddr_interfaces._dict() == {
        "iface Desc": "00:00:00:00:01"
    }
    assert mock_hub.grains.GRAINS.ip4_interfaces._dict() == {
        "iface Desc": ("192.168.2.1",)
    }
    assert mock_hub.grains.GRAINS.ip6_interfaces._dict() == {"iface Desc": ("fe80::2",)}
    assert mock_hub.grains.GRAINS.ip_interfaces._dict() == {
        "iface Desc": ("192.168.2.1", "fe80::2")
    }


@pytest.mark.asyncio
async def test_load_gateway_false(mock_hub, hub):
    mock_hub.exec.wmi.get.return_value = []
    mock_hub.grains.windows.net.interfaces.load_interfaces = (
        hub.grains.windows.net.interfaces.load_interfaces
    )
    await mock_hub.grains.windows.net.interfaces.load_interfaces()

    assert mock_hub.grains.GRAINS.ip4_gw is False
    assert mock_hub.grains.GRAINS.ip6_gw is False
    assert mock_hub.grains.GRAINS.ip_gw is False

    assert mock_hub.grains.GRAINS.ipv4 == ()
    assert mock_hub.grains.GRAINS.ipv6 == ()
