import pytest
import socket
import mock


@pytest.mark.asyncio
async def test_load_localhost(mock_hub, hub):
    with mock.patch("socket.gethostname", return_value="test_host"):
        mock_hub.grains.windows.net.fqdn.load_localhost = (
            hub.grains.windows.net.fqdn.load_localhost
        )
        await mock_hub.grains.windows.net.fqdn.load_localhost()

    assert mock_hub.grains.GRAINS.localhost == "test_host"


@pytest.mark.asyncio
async def test_load_fqdn(mock_hub, hub):
    with mock.patch.object(
        socket,
        "getaddrinfo",
        side_effect=[
            [],
            [(socket.AF_INET, 0, 0, "", ("192.168.1.2", 0))],
            [(socket.AF_INET6, 0, 0, "", ("fe80::71a9:ffff:ffff:ffff", 0, 0, 19))],
        ],
    ):
        with mock.patch("socket.getfqdn", return_value="test_host.local"):
            mock_hub.grains.windows.net.fqdn.load_fqdn = (
                hub.grains.windows.net.fqdn.load_fqdn
            )
            await mock_hub.grains.windows.net.fqdn.load_fqdn()

    assert mock_hub.grains.GRAINS.fqdn == "test_host.local"
    assert mock_hub.grains.GRAINS.host == "test_host"
    assert mock_hub.grains.GRAINS.fqdn_ip4 == ("192.168.1.2",)
    assert mock_hub.grains.GRAINS.fqdn_ip6 == ("fe80::71a9:ffff:ffff:ffff",)
    assert mock_hub.grains.GRAINS.fqdns == ("192.168.1.2", "fe80::71a9:ffff:ffff:ffff")
