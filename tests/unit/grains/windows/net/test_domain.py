import pytest
import win32net
import win32netcon
import mock


@pytest.mark.asyncio
async def test_load_windows_domain(mock_hub, hub):
    with mock.patch.object(
        win32net,
        "NetGetJoinInformation",
        return_value=["testdomain", win32netcon.NetSetupDomainName,],
    ):
        mock_hub.grains.windows.net.domain.load_windows_domain = (
            hub.grains.windows.net.domain.load_windows_domain
        )
        await mock_hub.grains.windows.net.domain.load_windows_domain()

    assert mock_hub.grains.GRAINS.windowsdomain == "testdomain"
    assert mock_hub.grains.GRAINS.windowsdomaintype == "Domain"
