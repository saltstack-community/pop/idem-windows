import pytest
import mock


@pytest.mark.asyncio
async def test_load_dns(mock_hub, hub):
    class resolver:
        domain = "lan."
        nameservers = ["192.168.1.1", "192.168.2.1", "::1", "fe80::1"]
        search = ["test"]

    with mock.patch("dns.resolver.Resolver", return_value=resolver()):
        mock_hub.grains.windows.net.dns.load_dns = hub.grains.windows.net.dns.load_dns
        await mock_hub.grains.windows.net.dns.load_dns()

    assert mock_hub.grains.GRAINS.dns.nameservers == (
        "192.168.1.1",
        "192.168.2.1",
        "::1",
        "fe80::1",
    )
    assert mock_hub.grains.GRAINS.dns.ip4_nameservers == ("192.168.1.1", "192.168.2.1")
    assert mock_hub.grains.GRAINS.dns.ip6_nameservers == ("::1", "fe80::1")
    assert mock_hub.grains.GRAINS.dns.sortlist == (
        "192.168.1.0/24",
        "192.168.2.0/24",
        "::1/128",
        "fe80::1/128",
    )
    assert mock_hub.grains.GRAINS.dns.domain == "lan."
    assert mock_hub.grains.GRAINS.dns.search == ("test",)
    assert mock_hub.grains.GRAINS.dns.options == {}
