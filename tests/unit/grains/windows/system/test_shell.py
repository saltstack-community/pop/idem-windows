import os
import pytest
import mock


@pytest.mark.asyncio
async def test_load_shell(mock_hub, hub):
    ret = r"C:\Windows\system32\test_shell.exe"
    with mock.patch.dict(os.environ, {"COMSPEC": ret}):
        mock_hub.grains.windows.system.shell.load_shell = (
            hub.grains.windows.system.shell.load_shell
        )
        await mock_hub.grains.windows.system.shell.load_shell()
    assert mock_hub.grains.GRAINS.shell == ret
