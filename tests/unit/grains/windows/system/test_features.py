import pytest


@pytest.mark.asyncio
async def test_load_features(mock_hub, hub):
    class Feature:
        Name = "Microsoft-Windows-Subsystem-Linux"
        InstallState = 1

    mock_hub.exec.wmi.get.return_value = [Feature()]

    mock_hub.grains.windows.system.features.load_features = (
        hub.grains.windows.system.features.load_features
    )

    await mock_hub.grains.windows.system.features.load_features()

    assert mock_hub.grains.GRAINS.wsl is True
