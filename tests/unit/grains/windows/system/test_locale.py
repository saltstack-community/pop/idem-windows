import pytest
import mock


@pytest.mark.asyncio
async def test_load_locale(mock_hub, hub):
    with mock.patch(
        "locale.getdefaultlocale", return_value=("test_language", "test_encoding")
    ):
        with mock.patch(
            "sys.getdefaultencoding", return_value="test_detected_encoding"
        ):
            mock_hub.grains.windows.system.locale.load_locale = (
                hub.grains.windows.system.locale.load_locale
            )
            await mock_hub.grains.windows.system.locale.load_locale()

    assert mock_hub.grains.GRAINS.locale_info.defaultlanguage == "test_language"
    assert mock_hub.grains.GRAINS.locale_info.defaultencoding == "test_encoding"
    assert (
        mock_hub.grains.GRAINS.locale_info.detectedencoding == "test_detected_encoding"
    )
    assert mock_hub.grains.GRAINS.locale_info.detectedencoding
