import pytest


@pytest.mark.asyncio
async def test_load_console_user(mock_hub, hub):
    class Systeminfo:
        UserName = "test_login_user"

    mock_hub.exec.wmi.get.return_value = Systeminfo()
    mock_hub.exec.user.sid_from_name.return_value = (
        "1-5-21-992878714-404123874-2616370337-2999"
    )

    mock_hub.grains.windows.proc.user.load_console_user = (
        hub.grains.windows.proc.user.load_console_user
    )

    await mock_hub.grains.windows.proc.user.load_console_user()

    assert mock_hub.grains.GRAINS.console_username == "test_login_user"
    assert mock_hub.grains.GRAINS.console_user == 2999


@pytest.mark.asyncio
async def test_load_user(mock_hub, hub):
    mock_hub.exec.user.current.return_value = "test_user"
    mock_hub.exec.user.groups.return_value = ["Administrators", "foo", "bar"]
    mock_hub.exec.user.sid_from_name.side_effect = [
        "1-5-21-992878714-404123874-2616370337-2999",
        "S-1-5-32-544",
    ]

    mock_hub.grains.windows.proc.user.load_user = hub.grains.windows.proc.user.load_user

    await mock_hub.grains.windows.proc.user.load_user()

    assert mock_hub.grains.GRAINS.username == "test_user"
    assert mock_hub.grains.GRAINS.uid == 2999
    assert mock_hub.grains.GRAINS.groupname == "Administrators"
    assert mock_hub.grains.GRAINS.gid == 544
