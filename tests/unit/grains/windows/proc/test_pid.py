import pytest
import mock


@pytest.mark.asyncio
async def test_load_pid(mock_hub, hub):
    with mock.patch("os.getpid", return_value=99999):
        mock_hub.grains.windows.proc.pid.load_pid = hub.grains.windows.proc.pid.load_pid
        await mock_hub.grains.windows.proc.pid.load_pid()
    assert mock_hub.grains.GRAINS.pid == 99999
