import pytest


@pytest.mark.asyncio
async def test_load_gpu_data(mock_hub, hub):
    ret = lambda: 0
    ret.Name = "testgpu"
    ret.AdapterCompatibility = "testvendor"

    ret1 = lambda: 0
    ret1.Name = "testgpu1"
    ret1.AdapterCompatibility = "testvendor1"

    mock_hub.exec.wmi.get.return_value = [ret, ret1]
    mock_hub.grains.windows.hw.gpu.load_gpu_data = (
        hub.grains.windows.hw.gpu.load_gpu_data
    )
    await mock_hub.grains.windows.hw.gpu.load_gpu_data()

    assert mock_hub.grains.GRAINS.gpus == (
        {"model": "testgpu", "vendor": "testvendor"},
        {"model": "testgpu1", "vendor": "testvendor1"},
    )
    assert mock_hub.grains.GRAINS.num_gpus == 2
