import pytest

WMIC_DATA = """
DeviceId  MediaType
4         4
2         0
1         4
0         3
3         4
"""


@pytest.mark.asyncio
async def test_load_disks(mock_hub, hub):
    mock_hub.exec.wmic.get.return_value = WMIC_DATA
    mock_hub.grains.windows.hw.disks.load_disks = hub.grains.windows.hw.disks.load_disks
    await mock_hub.grains.windows.hw.disks.load_disks()

    assert mock_hub.grains.GRAINS.disks == (
        "\\\\.\\PhysicalDrive0",
        "\\\\.\\PhysicalDrive1",
        "\\\\.\\PhysicalDrive2",
        "\\\\.\\PhysicalDrive3",
        "\\\\.\\PhysicalDrive4",
    )
    assert mock_hub.grains.GRAINS.SSDs == (
        "\\\\.\\PhysicalDrive1",
        "\\\\.\\PhysicalDrive3",
        "\\\\.\\PhysicalDrive4",
    )


@pytest.mark.asyncio
async def test_load_fibre_channel(mock_hub, hub):
    pass
    # TODO ONCE The grain works this test needs to be written


@pytest.mark.asyncio
async def test_load_iqn(mock_hub, hub):
    mock_hub.exec.wmic.get.return_value = "iqn.test\niqn.test1\niqn.test2"
    mock_hub.grains.windows.hw.disks.load_iqn = hub.grains.windows.hw.disks.load_iqn
    await mock_hub.grains.windows.hw.disks.load_iqn()

    assert mock_hub.grains.GRAINS.iscsi_iqn == ("iqn.test", "iqn.test1", "iqn.test2")
