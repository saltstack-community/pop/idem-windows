import os
import pytest
import mock


@pytest.mark.asyncio
async def test_load_cpu_model(mock_hub, hub):
    ret = "Test Value"
    mock_hub.exec.reg.read_value.return_value = {"vdata": ret}
    mock_hub.grains.windows.hw.cpu.load_cpu_model = (
        hub.grains.windows.hw.cpu.load_cpu_model
    )
    await mock_hub.grains.windows.hw.cpu.load_cpu_model()

    assert mock_hub.grains.GRAINS.cpu_model == ret


@pytest.mark.asyncio
async def test_load_num_cpus(mock_hub, hub):
    with mock.patch.dict(os.environ, {"NUMBER_OF_PROCESSORS": "99999"}):
        mock_hub.grains.windows.hw.cpu.load_num_cpus = (
            hub.grains.windows.hw.cpu.load_num_cpus
        )
        await mock_hub.grains.windows.hw.cpu.load_num_cpus()

    assert mock_hub.grains.GRAINS.num_cpus == 99999
