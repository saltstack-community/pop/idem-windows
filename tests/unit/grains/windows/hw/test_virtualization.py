import pytest


@pytest.mark.asyncio
async def test_load_hw_virt_enabled(mock_hub, hub):
    mock_hub.exec.wmi.get.return_value = True
    mock_hub.grains.windows.hw.virtualization.load_hw_virt_enabled = (
        hub.grains.windows.hw.virtualization.load_hw_virt_enabled
    )
    await mock_hub.grains.windows.hw.virtualization.load_hw_virt_enabled()

    assert mock_hub.grains.GRAINS.hardware_virtualization is True
