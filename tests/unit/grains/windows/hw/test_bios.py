import pytest


@pytest.mark.asyncio
async def test_load_bios_info(mock_hub, hub):
    ret = lambda: 0
    ret.Name = "A.A0"
    ret.ReleaseDate = "20190618000000.000000+000"
    ret.SerialNumber = "TESTSERIALNUMBER"

    mock_hub.exec.wmi.get.return_value = ret
    mock_hub.grains.windows.hw.bios.load_bios_info = (
        hub.grains.windows.hw.bios.load_bios_info
    )
    await mock_hub.grains.windows.hw.bios.load_bios_info()

    assert mock_hub.grains.GRAINS.biosversion == "A.A0"
    assert mock_hub.grains.GRAINS.biosreleasedate == "6/18/2019"
    assert mock_hub.grains.GRAINS.serialnumber == "TESTSERIALNUMBER"
