import pytest


@pytest.mark.asyncio
async def test_load_motherboard(mock_hub, hub):
    ret = lambda: 0
    ret.Product = "testproduct"
    ret.SerialNumber = "testserial"

    mock_hub.exec.wmi.get.return_value = ret
    mock_hub.grains.windows.hw.motherboard.load_motherboard = (
        hub.grains.windows.hw.motherboard.load_motherboard
    )
    await mock_hub.grains.windows.hw.motherboard.load_motherboard()

    assert mock_hub.grains.GRAINS.motherboard.productname == "testproduct"
    assert mock_hub.grains.GRAINS.motherboard.serialnumber == "testserial"


@pytest.mark.asyncio
async def test_load_system_info(mock_hub, hub):
    ret = lambda: 0
    ret.Manufacturer = "Micro-Star International Co., Ltd."
    ret.Model = "TEST-1234"
    ret.Name = "TEST-NAME"

    mock_hub.exec.wmi.get.return_value = ret
    mock_hub.grains.windows.hw.motherboard.load_system_info = (
        hub.grains.windows.hw.motherboard.load_system_info
    )
    await mock_hub.grains.windows.hw.motherboard.load_system_info()

    assert mock_hub.grains.GRAINS.manufacturer == "Micro-Star International Co., Ltd."
    assert mock_hub.grains.GRAINS.productname == "TEST-1234"
    assert mock_hub.grains.GRAINS.computer_name == "TEST-NAME"
