import pytest
import mock


@pytest.mark.asyncio
async def test_load_cpuarch(mock_hub, hub):
    with mock.patch("platform.machine", return_value="test"):
        mock_hub.grains.windows.hw.platform.load_cpuarch = (
            hub.grains.windows.hw.platform.load_cpuarch
        )
        await mock_hub.grains.windows.hw.platform.load_cpuarch()

    assert mock_hub.grains.GRAINS.cpuarch == "test"


@pytest.mark.asyncio
async def test_load_kernel_version(mock_hub, hub):
    with mock.patch("platform.node", return_value="test"):
        mock_hub.grains.windows.hw.platform.load_nodename = (
            hub.grains.windows.hw.platform.load_nodename
        )
        await mock_hub.grains.windows.hw.platform.load_nodename()

    assert mock_hub.grains.GRAINS.nodename == "test"


@pytest.mark.asyncio
async def test_load_nodename(mock_hub, hub):
    with mock.patch("platform.version", return_value="test"):
        mock_hub.grains.windows.hw.platform.load_kernel_version = (
            hub.grains.windows.hw.platform.load_kernel_version
        )
        await mock_hub.grains.windows.hw.platform.load_kernel_version()

    assert mock_hub.grains.GRAINS.kernelversion == "test"
