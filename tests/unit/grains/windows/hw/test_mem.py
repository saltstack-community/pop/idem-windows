import pytest
import mock


@pytest.mark.asyncio
async def test_load_memdata(mock_hub, hub):
    with mock.patch(
        "win32api.GlobalMemoryStatusEx", return_value={"TotalPhys": 9999999999}
    ):
        mock_hub.grains.windows.hw.mem.load_memdata = (
            hub.grains.windows.hw.mem.load_memdata
        )
        await mock_hub.grains.windows.hw.mem.load_memdata()

    assert mock_hub.grains.GRAINS.mem_total == 9536


@pytest.mark.asyncio
async def test_load_swapdata(mock_hub, hub):
    ret = lambda: 0
    ret.AllocatedBaseSize = 9999

    mock_hub.exec.wmi.get.return_value = ret
    mock_hub.grains.windows.hw.mem.load_swapdata = (
        hub.grains.windows.hw.mem.load_swapdata
    )
    await mock_hub.grains.windows.hw.mem.load_swapdata()

    assert mock_hub.grains.GRAINS.swap_total == 9999
