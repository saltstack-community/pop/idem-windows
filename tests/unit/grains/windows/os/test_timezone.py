import pytest


@pytest.mark.asyncio
async def test_load_timezone(mock_hub, hub):
    class Timeinfo:
        Description = "test_timezone"

    mock_hub.exec.wmi.get.return_value = Timeinfo()
    mock_hub.grains.windows.os.timezone.load_timezone = (
        hub.grains.windows.os.timezone.load_timezone
    )
    await mock_hub.grains.windows.os.timezone.load_timezone()

    assert mock_hub.grains.GRAINS.timezone == "test_timezone"
