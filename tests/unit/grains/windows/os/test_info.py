import pytest


@pytest.mark.asyncio
async def test_load_build(mock_hub, hub):
    mock_hub.exec.reg.read_value.side_effect = [
        {"vdata": "100"},
        {"vdata": "111"},
    ]
    mock_hub.grains.windows.os.info.load_build = hub.grains.windows.os.info.load_build
    await mock_hub.grains.windows.os.info.load_build()

    assert mock_hub.grains.GRAINS.osbuild == "100.111"


@pytest.mark.asyncio
async def test_load_build_version(mock_hub, hub):
    mock_hub.exec.reg.read_value.return_value = {"vdata": "1801"}
    mock_hub.grains.windows.os.info.load_build_version = (
        hub.grains.windows.os.info.load_build_version
    )
    await mock_hub.grains.windows.os.info.load_build_version()
    assert mock_hub.grains.GRAINS.osbuildversion == 1801


@pytest.mark.asyncio
async def test_load_codename(mock_hub, hub):
    mock_hub.exec.reg.read_value.return_value = {"vdata": "bedrock"}
    mock_hub.grains.windows.os.info.load_codename = (
        hub.grains.windows.os.info.load_codename
    )
    await mock_hub.grains.windows.os.info.load_codename()
    assert mock_hub.grains.GRAINS.oscodename == "bedrock"


@pytest.mark.asyncio
async def test_load_osinfo(mock_hub, hub):
    class osinfo:
        Manufacturer = "testman"
        Caption = "Windows Test Server 2099 R6"
        Version = "99.99.99"
        CSDVersion = "Service Pack 55"
        OSArchitecture = "testarch"
        ProductType = 0

    mock_hub.exec.wmi.get.return_value = osinfo()
    mock_hub.grains.windows.os.info.load_osinfo = hub.grains.windows.os.info.load_osinfo
    await mock_hub.grains.windows.os.info.load_osinfo()

    assert mock_hub.grains.GRAINS.os == "Windows"
    assert mock_hub.grains.GRAINS.osmanufacturer == "testman"
    assert mock_hub.grains.GRAINS.osfullname == "Windows Test Server 2099 R6"
    assert mock_hub.grains.GRAINS.kernelrelease == "99.99.99"
    assert mock_hub.grains.GRAINS.osversion == "99.99.99"
    assert mock_hub.grains.GRAINS.osrelease == "2099ServerR6"
    assert mock_hub.grains.GRAINS.osfinger == "Windows-2099ServerR6"
    assert mock_hub.grains.GRAINS.osrelease_info == (99, 99, 99)
    assert mock_hub.grains.GRAINS.osservicepack == "Service Pack 55"
    assert mock_hub.grains.GRAINS.osarch == "testarch"


@pytest.mark.asyncio
async def test_load_osmajorrelease(mock_hub, hub):
    mock_hub.exec.reg.read_value.return_value = {"vdata": "99"}
    mock_hub.grains.windows.os.info.load_osmajorrelease = (
        hub.grains.windows.os.info.load_osmajorrelease
    )
    await mock_hub.grains.windows.os.info.load_osmajorrelease()
    assert mock_hub.grains.GRAINS.osmajorrelease == 99
