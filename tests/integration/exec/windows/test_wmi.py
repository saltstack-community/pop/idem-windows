import asyncio
import pytest


async def _get_prop(hub, subtests, class_, prop, index: int = None):
    with subtests.test(property_=prop):
        hub.exec.wmi.get(class_, index, prop)


async def _get_class(hub, subtests, class_):
    coros = []
    with subtests.test(class_=class_):
        c = await hub.exec.wmi.get(class_)
        if isinstance(c, list):
            if not len(c) == 1:
                return
            for prop in c[0].properties:
                coros.append(_get_prop(hub, subtests, class_, prop, 0))

    if hasattr(c, "properties"):
        for prop in c.properties:
            coros.append(_get_prop(hub, subtests, class_, prop, None))
    return await asyncio.wait(*coros, return_when=asyncio.FIRST_EXCEPTION)


@pytest.mark.expensive_test
async def test_get(hub, subtests):
    coros = []
    for class_ in hub.exec.wmi.WMI.classes:
        coros.append(_get_class(hub, subtests, class_))
    assert (
        not (await asyncio.wait(coros, return_when=asyncio.FIRST_EXCEPTION))[0]
        .pop()
        .exception()
    )
