import asyncio
import pytest


@pytest.fixture(scope="session")
def hub(hub):
    hub.pop.sub.add(dyne_name="grains")
    hub.pop.sub.add(dyne_name="exec")
    hub.pop.sub.add(dyne_name="states")

    yield hub


@pytest.fixture(scope="session")
def event_loop(hub):
    """
    Override the pytest-asyncio loop with the ProactorEventLoop on the hub
    """
    loop = hub.pop.Loop
    assert isinstance(loop, asyncio.ProactorEventLoop), "Invalid event loop for windows"
    yield loop
